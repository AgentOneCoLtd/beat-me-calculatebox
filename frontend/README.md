# backend project

----

## prefer tech stacks
- `typescript@^4`
- framework
  - `react@^17`
  - `vue@^3`
- zero configuration framwork / setup
  - nextjs
  - nuxtjs
  - as you wish 
- UI library / component
  - tailwindcss
  - antd
  - any as you want
- test library
  - jest, ts-jest
- code format
  - eslint
  - prettier